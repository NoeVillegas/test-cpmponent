{
  const {
    html,
  } = Polymer;
  /**
    `<test-component>` Description.

    Example:

    ```html
    <test-component></test-component>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --test-component | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class TestComponent extends Polymer.Element {

    static get is() {
      return 'test-component';
    }

    static get properties() {
      return {};
    }
  }

  customElements.define(TestComponent.is, TestComponent);
}